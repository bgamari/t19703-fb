#!/usr/bin/env python3

import networkx as nx
from typing import Set, TextIO, NewType
from textwrap import dedent

ModName = NewType("ModName", str)

def write_file(fname: str, contents: str):
    print(contents, file=open(fname, 'w'))

def gen_module(mod_name: ModName, imports: Set[ModName]):
    write_file(f'{mod_name}.hs', dedent(f'''
        {{-# LANGUAGE TypeFamilies #-}}
        module {mod_name} where
        import Fam
    ''') + '\n'.join(f'import {m} ()' for m in imports) +
    dedent('''
        data T = T
        type instance Fam T = T
    '''))

def nodeModule(n: int) -> ModName:
    return ModName(f'M{n}')

def gen_graph(gr: nx.DiGraph) -> Set[ModName]:
    modules = set()
    for n in gr.nodes:
        mod_name = nodeModule(n)
        gen_module(mod_name, set(nodeModule(m) for m in gr.successors(n)))
        modules.add(mod_name)

    return modules

def gen_join(mod_name: ModName, imports: Set[ModName]):
    write_file(
        f'{mod_name}.hs',
        f'module {mod_name} where\n' + '\n'.join(f'import {m}' for m in imports)
    )

def gen(dot: TextIO) -> None:
    gr = nx.DiGraph(nx.drawing.nx_pydot.read_dot(dot))
    modules = gen_graph(gr)
    gen_join('Top', modules)
    print(f'Generated {len(gr.nodes)} nodes, {len(gr.edges)} edges')

def main() -> None:
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('dot_file', type=argparse.FileType('r'))
    args = p.parse_args()
    gen(args.dot_file)

main()
