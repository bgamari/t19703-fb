#!/usr/bin/env bash
set -e

GHC="${GHC:-ghc}"

mkdir -p tmp
cp Fam.hs tmp/
cd tmp
if [ ! -f Top.hs ]; then
    python3 ../gen.py ../minimal.dot
fi

rm -f *.hi *.o
"$GHC" Top.hs +RTS -p -s -RT
mv ghc.prof ..
